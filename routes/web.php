<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', ['as' => 'home', 'uses' => 'FrontEnd\HomeController@index']);
// Routes Pagine Statiche
Route::get('/chi-siamo', ['as' => 'about', 'uses' => 'FrontEnd\StaticPagesController@ChiSiamo']);
Route::get('/privacy-policy',['as' => 'privacy', 'uses' => 'FrontEnd\StaticPagesController@privacy' ]);
// Routes Contatto
Route::get('/contatti', [ 'as' => 'contact', 'uses' => 'FrontEnd\ContattiController@Contact']);
Route::post('/contatti', [ 'as' => 'contactPost', 'uses' => 'FrontEnd\ContattiController@ContactPost']);
// Route Servizi
Route::get('/Raccolta-e-trasporto', ['as' => 'servizio', 'uses' => 'FrontEnd\StaticPagesController@Servizio']);
Route::get('/Stoccaggio', ['as' => 'servizio1', 'uses' => 'FrontEnd\StaticPagesController@Servizio_1']);
Route::get('/Intermediazione', ['as' => 'servizio2', 'uses' => 'FrontEnd\StaticPagesController@Servizio_2']);
Route::get('/Consulenza-ambientale', ['as' => 'servizio3', 'uses' => 'FrontEnd\StaticPagesController@Servizio_3']);
// Route Autorizzazioni
Route::get('/autorizzazioni', ['as' => 'autorizzazioni', 'uses' => 'FrontEnd\AutorizzazioniController@index']);
Route::get('/autorizzazioni/download/{id}', ['as' => 'autorizzazionipdf', 'uses' => 'FrontEnd\AutorizzazioniController@download']);
Route::get('/moduli/download/{id}', ['as' => 'modulipdf', 'uses' => 'FrontEnd\AutorizzazioniController@downloadmodulo']);

/*
 Route::get('/servizi',['as' => 'servizi', 'uses' => 'FrontEnd\ServiziController@index']);
Route::get('/servizi/{service_slug}',['as' => 'servizio', 'uses' => 'FrontEnd\ServiziController@servizio']);
Route::get('/servizi/download/{$id}',['as' => 'servizio', 'uses' => 'FrontEnd\ServiziController@downloadpdf']);
*/

