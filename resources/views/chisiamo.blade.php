
@extends('master')

@section('content')

<div class="page-header has-image">
            <div class="page-header-content">
                <div class="featured-image-chi-siamo"></div>
                <div class="container">
                    <h1>CHI SIAMO</h1>
                    <nav class="breadcrumbs">
                        <a class="home" href="#"><span>Home</span></a>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span>CHI SIAMO</span>
                    </nav>
                </div>
            </div>
</div>

<div class="service-dtail">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="dtlbgimg">
                            <img src="{{asset('images/pacorig-company.jpg')}}" alt="Pacorig F.lli - Immagine dello stabilimento" />
                        </div>

                        <div class="mf-section-title text-left dark medium-size bold margbtm40">
                            <h2> La nostra Azienda</h2>
                        </div>
                        <p>La storia della nostra azienda ha inizio nel lontano 1957 quando la famiglia Pacorig dà il via all’attività di commercio di rottami ferrosi e metalli.</p>
                        <p>A seguito della sempre maggiore sensibilità rivolta alle problematiche ambientali ed intuendo la necessità di fornire risposte efficaci in tal senso, nel 1992 viene dato l’avvio alle pratiche per l’ottenimento dell’autorizzazione allo stoccaggio di rifiuti che viene conseguita nel 1994. A partire da tale data all’attività di recupero di rottami metallici viene affiancata quella di raccolta e stoccaggio di rifiuti non pericolosi.</p>
                        <p>Operando principalmente per conto delle aziende presenti all’interno del cosiddetto “Distretto della sedia”, le tipologie di rifiuti trattate all’interno dell’impianto di stoccaggio sono prevalentemente composte dagli scarti misti di magazzino quali gli imballaggi in plastica, legno, carta e cartone, ferro ecc. e i residui provenienti dalle attività di verniciatura quali le morchie e i filtri delle cabine di verniciatura e gli imballaggi plastici e/o metallici.</p>
                        <p>All’attività di stoccaggio è da sempre affiancata quella dei trasporti effettuati con automezzi di proprietà debitamente autorizzati alle categorie 4 (rifiuti non pericolosi) e 5 (rifiuti pericolosi). Tutti i mezzi sono dotati di impianto scarrabile per la movimentazione di cassoni e di gru idraulica per il carico dei rifiuti sfusi. La nostra azienda è dotata inoltre di varie attrezzature per il contenimento di rifiuti che comprendono tra gli altri circa 100 cassoni la maggior parte dei quali a noleggio presso i nostri clienti.</p>
                        <p>In alternativa al noleggio di cassoni di varie misure ci occupiamo anche della fornitura di imballaggi per rifiuti sfusi quali fusti metallici e sacchi big-bags di vario genere per rifiuti non pericolosi e pericolosi.</p>
                        <p>Alle succitate attività di trasporto e stoccaggio rifiuti viene affiancata nell’anno 2012 quella di intermediazione di rifiuti senza detenzione per tutte le tipologie di materiali non trattate direttamente presso il nostro impianto.</p>
                        <p>Per ultima ma non meno importante nell’anno 1999 vede la luce la ditta GEA di Pacorig Manuela la cui attività si rivolge alla risoluzione di tutte le problematiche di carattere amministrativo inerenti il settore ambientale quali ad esempio la tenuta dei registri di carico e scarico rifiuti, la redazione del MUD, la redazione di pratiche autorizzative (emissioni in atmosfera, inscrizione CONAI, iscrizione all’Albo Gestori Ambientali, ecc.) e relative a sistemi di gestione, audit, conformità normativa ecc.</p>
                        <hr>
                    </div>

                    <!-- sidebar -->
                    <aside class="widgets-area primary-sidebar service-sidebar ol-sm-12 col-md-4">
                        <div class="induscity-widget">
                            <div class="widget services-menu-widget">
                                <h4 class="widget-title">Le nostre attività</h4>
                                <ul class="menu service-menu">
                                    <li><a href="#"><i class="fa fa-long-arrow-right"></i>Raccolta, stoccaggio e recupero rottami ferrosi e metalli</a></li>
                                    <li><a href="#"><i class="fa fa-long-arrow-right"></i>Raccolta, trasporto, stoccaggio e smaltimento rifiuti speciali non pericolosi</a></li>
                                    <li><a href="#"><i class="fa fa-long-arrow-right"></i>Intermdediazione</a></li>
                                    <li><a href="#"><i class="fa fa-long-arrow-right"></i>Consulenza ambientale</a></li>
                                    
                                </ul>
                            </div>
                            
                            <div class="widget_text widget">
                                <h4 class="widget-title"> I nostri contatti</h4>
                                <div class="textwidget">
                                    <div class="mf-team-contact">
                                        <p>Contattaci per ogni informazione che desideri avere</p>
                                        <div class="mf-contact phone">
                                            <i class="flaticon-tool"></i>
                                            <span class="semi-bold">Chiamaci:</span> +(39).0432.750721
                                        </div>
                                        <div class="mf-contact email">
                                            <i class="flaticon-note"></i>
                                            <span class="semi-bold">Scrivici:</span> <a href="mailto:info@pacorig.com">info@pacorig.com</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="widget mf-button-widget">
                                <a href="{{ route('contact') }}" class="mf-btn mf-btn-widget mf-btn-fullwidth">Richiedi maggiori informazioni</a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>

@endsection