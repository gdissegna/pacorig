@extends('master')

@section('content')

    <div class="page-header has-image">
        <div class="page-header-content">
            <div class="featured-image-consulenza-ambientale"></div>
            <div class="container">
                <h1>PRIVACY</h1>
                <nav class="breadcrumbs">
                    <a class="home" href="#"><span>Home</span></a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <span></span>
                </nav>
            </div>
        </div>
    </div>

    <div class="service-dtail">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    

                    <div class="mf-section-title text-left dark medium-size bold margbtm40">
                        <h2> Informativa prevista dal Reg. UE 2016/679</h2>
                    </div>
                    <p>Il Regolamento UE 2016/679 (nel prosieguo, “GDPR”) nonché altri disposti normativi (tra cui, il D.Lgs. 196/2003 nel prosieguo, “Codice Privacy”) e Regolamentari, tutelano la riservatezza dei dati personali ed impongono una serie di obblighi a chi tratta informazioni personali riferite a persone fisiche, definite interessati. Tra i più importanti adempimenti che il GDPR impone, vi è quello di informare gli interessati ed acquisirne, laddove richiesto quale base giuridica, il consenso al trattamento.</p>
                    <p>La presente informativa fa riferimento al trattamento dei dati personali – forniti direttamente dall’impresa, dall’ente, dal professionista e/o acquisiti presso terzi – per le procedure legate alla partecipazione del Titolare ad un appalto o alla presentazione (e successiva valutazione) di un’offerta ma anche per la stipula e l’esecuzione di un contratto (di appalto/fornitura o di altro genere), i connessi adempimenti e le attività amministrativo-contabili correlate.</p>
                    <p>Per le finalità sopra indicate, potranno o dovranno essere raccolti e trattati dati personali relativi ai rappresentanti legali, ai responsabili tecnici, ai dipendenti/collaboratori della controparte e agli eventuali subappaltatori.</p>
                    <p>
                    Premesso che il trattamento dati operato dal Titolare sarà improntato ai principi di liceità, correttezza e trasparenza, di minimizzazione e limitazione della conservazione dei dati, di esattezza, integrità e riservatezza, si forniscono le seguenti informazioni:
                    </p>
                    <p>
                        a) Titolare del Trattamento è il Pacorig F.lli s.a.s. di Bruno Pacorig & C. – via Trieste nr. 4 – 33044 Manzano (UD) – tel. 0432 750721 – mail amministrazione@pacorig.com</p>
                    <p>b) i dati da forniti o acquisiti presso terzi, verranno trattati lecitamente grazie alla base giuridica e unicamente per le finalità descritte in tabella seguente:</p>
                
                    <p>&nbsp;</p>
                    <table border="1" width="100%">
                     <tr>
                        <td bgcolor="#dedede" width="80%" align="center">
                          <b>Finalità</b>
                         </td>
                        <td bgcolor="#dedede" width="20%" align="center">
                          <b>Base giuridica GDPR</b>
                         </td>                
                        </tr>
                        <tr>
                        <td>
                            1) dar corso ad attività preliminari, verificare l’idoneità tecnica, economica e finanziaria e la sussistenza di tutti i requisiti imposti dalla normativa applicabile, per consentire la successiva stipula ed esecuzione del contratto. Stipula ed esecuzione del contratto.
                            </td>
                            <td>
                                6.1.b –  Contratto;<br>
                                6.1.c –  Obbligo Legale;
                            </td>
                            
                        </tr>
                        <tr>
                        <td>
                            2) laddove richiesto dalla normativa, vengono trattati anche dati giudiziari legati ai rappresentanti legali e tecnici della società ai fini della verifica dell’assenza di cause di esclusione e dell’assenza di provvedimenti interdittivi per la società ex D.Lgs. 231/01 o Normativa Antimafia.
                            </td>
                            <td>
                                6.1.c –  Obbligo Legale;
                            </td>
                        </tr>
                        <tr>
                         <td>
                            3) Nel caso di cantieri o lavori in aree gestite dal Titolare, vengono trattati anche dati dei dipendenti/collaboratori con un ruolo nell’esecuzione del contratto, ai fini di gestione delle tematiche antinfortunistiche.
                            </td>
                         <td>
                            6.1.c –  Obbligo Legale;
                            </td>
                        </tr>
                        <tr>
                            <td>
                             4) Gestione contabile, amministrativa e finanziaria.
                            </td>
                            <td>
                             6.1.c –  Obbligo Legale;
                            </td>
                        </tr>
                        <tr>
                            <td>
                             5) Esercizio di un diritto in sede giudiziaria per la difesa degli interessi del Titolare.
                            </td>
                            <td>
                             6.1.f –  Legittimo interesse;
                            </td>
                        </tr>   
                    </table>
                    <p>&nbsp;</p>
                    <p>
                    
                    I dati verranno trattati sia su supporto cartaceo che magnetico, manualmente e/o con strumenti elettronici o, comunque, automatizzati e sono conservati per un periodo di 10 anni e, in caso di contenzioso, fino a che non sono divenuti definitivi i giudizi in ogni ordine e grado.
                    </p>
                    
                    <p>
                    c) Il conferimento dei dati è obbligatorio per tutto quanto richiesto ai fini degli adempimenti legali e contrattuali. I dati potranno essere trattati per dare seguito a richieste da parte dell'autorità amministrativa o giudiziaria competente e, più in generale, di soggetti pubblici nel rispetto degli obblighi di legge.</p>
                    <p>d) I dati potranno essere oggetto di trattamento da parte di soggetti qualificati come Responsabili ai sensi dell’art. 4.8 e 28 del GDPR (Professionisti con ruoli di controllo interno; Coordinatore per la sicurezza in fase di esecuzione, in caso di cantieri; Direttore dei Lavori, se nominato; Commercialisti; Società di consulenza e servizi; Società di assistenza hardware e software; …) sia da parte di soggetti (dipendenti e collaboratori a vario titolo) specificatamente autorizzati al trattamento ai sensi dell’art. 29 del GDPR, che operano sotto la diretta autorità del Titolare, il quale li ha istruiti in tal senso.</p>
                    <p>e) Premettendo che la comunicazione a terzi non esime questi ultimi dal fornire l’informativa e dal trattare lecitamente i dati solo in forza di una valida base giuridica, si precisa che, salvo le comunicazioni a soggetti cui la facoltà di accedere ai dati sia riconosciuta da disposizioni di Legge o da ordini di Autorità, i dati potranno essere comunicati a: Banche ed istituzioni Finanziarie; società di Leasing/Factoring; Assicurazioni e Broker; Professionisti e Società di Servizi; autorità competenti per adempiere ad obblighi di Legge e/o a disposizioni di organi pubblici.</p>
                    <p>f) La diffusione dei dati si limita all’eventuale pubblicazione, in ottemperanza ad obblighi di Legge;</p>
                    <p>g) Con riferimento ad eventuali servizi Cloud di cui il Titolare si avvale, i dati potranno essere trasferiti in un Paese terzo, unicamente in Paesi caratterizzati da un elevato standard di protezione dei dati personali oggetto di decisioni di adeguatezza da parte delle Autorità.</p>
                    <p>h) Rivolgendosi al Referente, in ogni momento sarà possibile esercitare i propri diritti ai sensi degli articoli da 15 a 22 del GDPR, ovvero il diritto di chiedere al titolare del trattamento l'accesso ai dati personali, la rettifica o la cancellazione degli stessi o la limitazione del trattamento che li riguarda, nonchè opporsi al trattamento. Presso il Referente si potrà anche richiedere la lista dei soggetti nominati Responsabili.</p>
                    <p>E’ prerogativa dell’interessato rivolgersi ad un’Autorità di controllo per proporre reclamo.
                    </p>
                    
                    
                    <hr>
                </div>

            </div>
        </div>
    </div>

@endsection