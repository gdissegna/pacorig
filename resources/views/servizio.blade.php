@extends('master')

@section('content')

 <!--page-header-->
    <div class="page-header has-image">
        <div class="page-header-content">
            <div class="featured-image-raccolta"></div>
            <div class="container">
                <h1>Raccolta e trasporto rottami ferrosi e metalli</h1>
                <nav class="breadcrumbs">
                    <a class="home" href="#"><span>Home</span></a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <span>Servizi</span>
                </nav>
            </div>
        </div>
    </div>
    <!--page-header  end-->

<!-- projects -->
    <div class="single-project pagepadd">
        <div class="container">
            <div class="single-project-wrapper single-portfolio">
                <div class="entry-thumbnail  single-slide">
                    <div class="item">
                        <a href="#" class="photoswipe"><img src="images/services/pacorig-mezzi-raccolta-trasporto-rifiuti-slide-1.jpg" alt="" /></a>
                    </div>
                    <div class="item">
                            <a href="#" class="photoswipe"><img src="images/services/pacorig-mezzi-raccolta-trasporto-rifiuti-slide-2.jpg" alt="" /></a>
                        </div>
                    <div class="item">
                            <a href="#" class="photoswipe"><img src="images/services/pacorig-raccolta-trasporto-rifiuti-cassone-slide-3.jpg" alt="" /></a>
                        </div>
                </div>

                <h2 class="single-project-title">Raccolta e trasporto rottami ferrosi e metalli</h2>
                <div class="row">
                    <div class="entry-content col-md-9 col-sm-12 col-xs-12">
                        <p>
                            L’attività di trasporto è effettuata con mezzi di proprietà debitamente autorizzati alla categoria 4 (rifiuti non pericolosi) e alla categoria 5 (rifiuti non pericolosi).</p>
                        <p>Tutti i mezzi sono dotati di impianto scarrabile per la movimentazione di cassoni e di gru idraulica per il carico dei rifiuti sfusi.</p>
                        <p>La nostra azienda è dotata inoltre di varie attrezzature per il contenimento di rifiuti che comprendono tra gli altri circa 100 cassoni scarrabili,  la maggior parte dei quali a noleggio presso i clienti.</p>
                        <p>In alternativa al noleggio di cassoni di varie misure ci occupiamo anche della fornitura di imballaggi per rifiuti sfusi quali fusti metallici e sacchi big-bags di vario genere per rifiuti non pericolosi e pericolosi.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- projects end -->

@endsection