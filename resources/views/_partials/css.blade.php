
<! -- bootstrap v3.3.6 css -->
    <link href = "css/bootstrap.css" rel = "stylesheet" >
    <! -- font-awesome css -->
    <link href = "css/font-awesome.css" rel = "stylesheet" >
    <! -- flaticon css -->
    <link href = "css/flaticon.css" rel = "stylesheet" >
    <! -- factoryplus-icons css -->
    <link href = "css/factoryplus-icons.css" rel = "stylesheet" >
    <! -- animate css -->
    <link href = "css/animate.css" rel = "stylesheet" >
    <! -- owl.carousel css -->
    <link href = "css/owl.css" rel = "stylesheet" >
    <! -- fancybox css -->
    <link href = "css/jquery.fancybox.css" rel = "stylesheet" >
    <link href = "css/hover.css" rel = "stylesheet" >
    <link href = "css/frontend.css" rel = "stylesheet" >
    <link href = "css/style.css" rel = "stylesheet" >
    <! -- switcher css -->
    <link href = "css/switcher.css" rel = "stylesheet" >
    <link rel = 'stylesheet' id = 'factoryhub-color-switcher-css' href = 'css/switcher/default.css' />
    <! -- revolution slider css -->

    <link rel = "stylesheet" type = "text/css" href = "{{ asset('css/revolution/settings.css') }}" >
    <link rel = "stylesheet" type = "text/css" href = "css/revolution/layers.css" >
    <link rel = "stylesheet" type = "text/css" href = "css/revolution/navigation.css" >
    <link href="css/responsive.css" rel="stylesheet">
 <! -- End -->