
<!-- jquery Liabrary -->
    <script src="js/jquery-1.12.4.min.js"></script>
    <!-- bootstrap v3.3.6 js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- fancybox js -->
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/jquery.fancybox-media.js"></script>
    <script src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"></script>
    <!-- owl.carousel js -->
    <script src="{{ asset('js/owl.js') }}"></script>
    <!-- counter js -->
    <script src="js/jquery.appear.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <!-- validate js -->
    <script src="js/validate.js"></script>
    <!-- switcher js -->
    <script src="js/switcher.js"></script>

    <!-- google map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq23y4jq5QFdzWerro1O86vCq6CgqlcVg"></script>
    <script src="js/gmap.js"></script>
    <script src="js/map-helper.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="js/revolution/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/revolution/extensions/revolution.extension.video.min.js"></script>

    <!-- Cookie Script -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/cookie-bar/cookiebar-latest.min.js?thirdparty=1&always=1"></script>

    <!-- script JS  -->
    <script src="js/scripts.min.js"></script>
    <script src="js/script.js"></script>

    {!! NoCaptcha::renderJs() !!}

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143797661-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-143797661-1');
</script>