

     <!-- Footer -->
        <div id="footer-widgets" class="footer-widgets widgets-area">
            <div class="container">
                <div class="row">
                    <div class="footer-sidebar footer-1 col-xs-12 col-sm-6 col-md-4">
                        <div class="widget_text widget">
                            <div class="textwidget">
                                <a href="#">
                                    <img src="images/logo.png" alt="Footer Logo">
                                </a>
                            </div>
                        </div>
                        <div class="widget_text widget">
                            <div class="textwidget">La ditta Pacorig nasce nel lontano 1957 come commercio di rottami ferrosi e metalli. Dal 1994 affianca l’attività di raccolta, trasporto e stoccaggio di rifiuti non pericolosi e pericolosi. Ci occupiamo anche di intermediazione di rifiuti non pericolosi e pericolosi trattati presso impianti terzi e della fornitura di servizi di assistenza e consulenza nel settore ambientale tramite la ditta GEA di Pacorig Manuela.</div>
                        </div>
                        
                    </div>
                    <div class="footer-sidebar footer-2 col-xs-12 col-sm-6 col-md-4">
                        <div class="widget widget_mf-custom-menu">
                            <h4 class="widget-title">Collegamenti rapidi</h4>
                            <div class="custom-menu-area">
                                <div class="custom-menu custom-menu-1">
                                    <ul class="menu">
                                        <li><a href="{{ route('about') }}" rel="Chi Siamo">Chi siamo</a></li>
                                        <li><a href="{{ route('autorizzazioni') }}" rel="Autorizzazioni">Autorizzazioni</a></li>
                                        <li><a href="{{ route('autorizzazioni') }}" rel="Modulistica">Modulistica</a></li>
                                        <li><a href="{{ route('contact') }}" rel="Contatti">Contatti</a></li>
                                        <li><a href="{{ route('privacy') }}" rel="Privacy">Privacy Policy</a></li>
                                    </ul>
                                </div>
                                <div class="custom-menu custom-menu-2">
                                    <ul class="menu">
                                        <li><a href="{{ route('servizio') }}" rel="Raccolta e Trasporto">Raccolta e trasporto</a></li>
                                        <li><a href="{{ route('servizio1') }}" rel="Stoccaggio">Stoccaggio</a></li>
                                        <li><a href="{{ route('servizio2') }}" rel="Intermediazione">Intermediazione</a></li>
                                        <li><a href="{{ route('servizio3') }}" rel="Consulenza ambientale">Consulenza ambientale</a></li>
                                    </ul>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-sidebar footer-3 col-xs-12 col-sm-6 col-md-4">
                        <div class="widget_text widget">
                            <h4 class="widget-title">Contatti</h4>
                            <div class="textwidget">
                                <div class="footer-widget-contact">
                                    <div class="detail address">
                                        <i class="flaticon-arrow"></i>
                                        <div>
                                            <span>Indirizzo:</span> Via Trieste n. 4, 33044 Manzano (UD)
                                                                                                              </div>
                                    </div>
                                    <div class="detail phone">
                                        <i class="flaticon-tool"></i>
                                        <div>
                                            <span>Chiamateci:</span> +(39) 0432.750721
                                                                                             </div>
                                    </div>
                                    <div class="detail mail">
                                        <i class="flaticon-note"></i>
                                        <div>
                                            <span>Scriveteci:</span> info@pacorig.com
                                                                            </div>
                                    </div>
                                    <div class="detail time">
                                        <i class="flaticon-wall-clock"></i>
                                        <div>
                                            <span>Orari:</span> Lunedì - Venerdì: 08:00-12:30 / 13:30-17:30
                                                                                              </div>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer end -->

        <!-- copyright -->
        <footer class="site-footer">
            <div class="container footer-info">
                <div class="footer-copyright">
                           Copyrights © 2019 All Rights Reserved by PACORIG / P. IVA e C.F. IT00151190303<br /> Cap. Soc. i.v. € 37.701,35 / Credits: <a href="#">Omega Web Agency</a>. </div>
                <div class="text-right">
                    
                </div>
            </div>
        </footer>

     <!--Scroll to top-->
     <a id="scroll-top" class="backtotop" href="#page-top"><i class="fa fa-angle-up"></i></a>

     <!--primary-mobile-nav-->
     <div class="primary-mobile-nav header-v1" id="primary-mobile-nav" role="navigation">
         <a href="#" class="close-canvas-mobile-panel">×</a>
         <ul class="menu">
             <li class="current-menu-item"><a href="{{ route('home') }}" class="dropdown-toggle">Home</a>
             </li>
             <li class="menu-item-button-link" ><a href="{{ route('about') }}" class="dropdown-toggle">Chi Siamo</a>

             </li>
             <li class="menu-item-has-children" ><a href="#" class="dropdown-toggle">Servizi</a>
                 <ul class="sub-menu">

                             <li><a href="{{ route('servizio') }}">Raccolta e trasporto</a></li>
                             <li><a href="{{ route('servizio1') }}">Stoccaggio</a></li>
                             <li><a href="{{ route('servizio2') }}">Intermediazione</a></li>
                             <li class="last-child"><a href="{{ route('servizio3') }}">Consulenza ambientale</a></li>
                 </ul>
             </li>
             <li class="menu-item-button-link" ><a href="{{ route('autorizzazioni') }}">Autorizzazioni</a></li>
             <li class="menu-item-button-link"><a href="{{ route('contact') }}">Contatti</a></li>

         </ul>

     </div>
     <div id="off-canvas-layer" class="off-canvas-layer"></div>
     <!--primary-mobile-nav end-->


@include('_partials.js')