@extends('master')

@section('content')

 <!--page-header-->
    <div class="page-header has-image">
        <div class="page-header-content">
            <div class="featured-image-intermediazione"></div>
            <div class="container">
                <h1>Intermediazione</h1>
                <nav class="breadcrumbs">
                    <a class="home" href="#"><span>Home</span></a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <span>Servizi</span>
                </nav>
            </div>
        </div>
    </div>
    <!--page-header  end-->

<!-- projects -->
    <div class="single-project pagepadd">
        <div class="container">
            <div class="single-project-wrapper single-portfolio">
                <div class="entry-thumbnail">
                    <div class="item">
                        <img src="images/services/pacorig-intermediazione-slide-1.jpg" alt="" />
                    </div>
                </div>

                <h2 class="single-project-title">Intermediazione</h2>
                <div class="row">
                    <div class="entry-content col-md-9 col-sm-12 col-xs-12">
                        <p>Con l’intento di ampliare sempre più l’offerta al fine di soddisfare a tutto tondo le esigenze di smaltimento dei nostri clienti, nell’anno 2012 viene affiancata alle altre attività quella di intermediazione rifiuti.</p>
                        <p>L’ottenimento di tale autorizzazione ci consente di gestire mediante partner affidabili i servizi di raccolta, trasporto e smaltimento di tutte le tipologie di rifiuti non gestibili direttamente presso il nostro impianto di stoccaggio.</p>
                        <p>In particolar modo ad essere interessati da tale attività sono quei rifiuti non pericolosi e pericolosi, solidi e liquidi, che necessitano di operazioni di trattamento presso impianti terzi.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- projects end -->

@endsection