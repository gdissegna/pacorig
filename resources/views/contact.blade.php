

@extends('master')

@section('content')

    <!--page-header-->
    <div class="page-header has-image">
        <div class="page-header-content">
            <div class="featured-image-contatti"></div>
            <div class="container">
                <h1>Contatti</h1>
                <nav class="breadcrumbs">
                    <a class="home" href="#"><span>Home</span></a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <span>Contatti</span>
                </nav>
            </div>
        </div>
    </div>
    <!--page-header  end-->

    <!-- team-->
    <div class="contactpage pagepadd">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="mf-contact-box clearfix ">
                        <div class="mf-section-title text-left dark medium-size ">
                            <h2>LA NOSTRA SEDE</h2></div>
                        <div class="contact-info address"><i class="flaticon-arrow"></i>
                            <div><span>Indirizzo:</span> Via Trieste n. 4, 33044 Manzano (UD)</div>
                        </div>
                        <div class="contact-info phone"><i class="fa fa-phone"></i>
                            <div><span>Telefono:</span> +(39) 0432.750721 </div>
                        </div>
                        <div class="contact-info email"><i class="flaticon-note"></i>
                            <div><span>E-mail:</span> info@pacorig.com</div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">

                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="mf-working-hour ">
                        <div class="mf-section-title text-left dark medium-size ">
                            <h2>I NOSTRI ORARI</h2></div>
                        <ul class="mf-list-hour">
                            <li><span class="day">Lunedì</span><span class="hour">8:00-12:30/13:30-17:30</span></li>
                            <li><span class="day">Martedì</span><span class="hour">8:00-12:30/13:30-17:30</span></li>
                            <li><span class="day">Mercoledì</span><span class="hour">8:00-12:30/13:30-17:30</span></li>
                            <li><span class="day">Giovedì</span><span class="hour">8:00-12:30/13:30-17:30</span></li>
                            <li><span class="day">Venerdì</span><span class="hour">8:00-12:30/13:30-17:30</span></li>
                            <li><span class="day">Sabato</span><span class="hour">Chiuso</span></li>
                            <li><span class="day">Domenica</span><span class="hour">Chiuso</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contactform">
        <div class="container">
            <div class="mf-section-title text-center dark large-size margbtm20">
                <h2>SCRIVETECI</h2>
            </div>
            <div class="text-center margbtm40">Non esitate a contattarci per ogni maggiore informazione 
                <br>il nostro Staff Vi risponderà nel più breve tempo possibile.</div>
            @if ($message = Session()->has('success') )
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
            <div class="contact-form mf-contact-form-7 form-light">
                <form method="post" action="{{ route('contactPost') }}" class="wpcf7-form" id="contact-form" novalidate="novalidate">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <div class="mf-form mf-form-2">
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-sm-12 mf-input-field">
                                <p>
                                    <input type="text" name="name" value="" size="40" placeholder="Nome">
                                </p>
                                <p>
                                    <input type="text" name="azienda" value="" size="40" placeholder="Azienda">
                                </p>
                                <p>
                                    <input type="text" name="indirizzo" value="" size="40" placeholder="Indirizzo">
                                </p>
                                <p>
                                    <input type="email" name="email" value="" size="40" placeholder="Email ">
                                </p>
                                <p>
                                    <input type="text" name="phone" value="" size="40" placeholder="Telefono">
                                </p>

                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12 mf-textarea-field">
                                <p>
                                    <input type="text" name="riferimento" value="" size="40" placeholder="Azienda di Riferimento">
                                </p>
                                <p>
                                    <input type="text" name="subject" value="" size="40" placeholder="Titolo Messaggio">
                                </p>
                                <p>
                                    <textarea name="message" cols="40" rows="40" placeholder="Inserite la Vostra richiesta"></textarea>
                                </p>
                            </div>
                            <div class="text-center mf-submit col-md-12 col-xs-12 col-sm-12">
                                <div class="row justify-content-md-center">
                                    <div class="col col-md-4">

                                    </div>
                                    <div class="col-md-4">
                                        {!! NoCaptcha::display() !!}
                                    </div>
                                    <div class="col col-md-4">

                                    </div>
                                </div>

                            </div>
                            <div class="text-center mf-submit col-md-12 col-xs-12 col-sm-12">
                                <button type="submit" class="btn-style-two">INVIA</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <!-- contact-->

    <!--google map    -->
    <div class="google-map-area">
        <div class="google-map" id="contact-google-map" data-map-lat="45.983761" data-map-lng="13.377501" data-icon-path="images/marker-1.png" data-map-title="Chester" data-map-zoom="13" data-markers='{
							"marker-1": [45.983761, 13.377501, "<h4>Pacorig F.lli</h4><p>Via Trieste, 4 - 33044 Manzano (UD) Italia</p>"]
						}' >
        </div>
    </div>

    <!--google map end-->

@endsection