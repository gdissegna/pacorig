@extends('master')

@section('title')

@section('content')

    <!-- detail sec -->
    <div class="service-dtail">
        <div class="container">
            <div class="row">
                <div class="col-md-12col-sm-12">
                    <div class="dtlbgimg">
                        <img src="images/services/8-big.jpg" alt=""/>
                    </div>
                    @foreach($autolist as $auto)
                        <div class="mf-section-title text-left dark medium-size bold col-md-12">
                            <h2>{{ $auto->name }}</h2>
                            <p>{!!  $auto->description !!}</p>
                        </div>
                        @foreach($auto->authorization as $documento)
                            <div class="col-md-12">
                                    <div class="mf-section-title text-left dark medium-size ">
                                        <div class="col-md-8 col-sm-12">
                                            <h2>{{ $documento->name }}</h2>
                                            <p>{!! $documento->description !!}</p>
                                        </div>
                                    </div>
                                    <aside class="widgets-area primary-sidebar service-sidebar ol-sm-12 col-md-4">
                                        <div class="widget_text widget">
                                            <h4 class="widget-title">Scarica PDF</h4>
                                            <div class="textwidget">
                                                <div class="download">
                                                    <div class="item-download">
                                                        <a href="http://www.pacorig.com/storage/{{ json_decode($documento->pdf)[0]->download_link }}"
                                                           target="_blank">
                                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>Download
                                                            PDF
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                            </div>
                        @endforeach
                    @endforeach
                    <hr>

                        <div class="mf-section-title text-left dark medium-size bold col-md-12 margtop40">
                            <h2>Modulistica</h2>
                            <p></p>
                        </div>
                        @foreach($modulilist as $modulo)
                            <div class="col-md-12">
                                <div class="mf-section-title text-left dark medium-size ">
                                    <div class="col-md-8 col-sm-12">
                                        <h2>{{ $modulo->name }}</h2>
                                        <p>{!! $modulo->description !!}</p>
                                    </div>
                                </div>
                                <aside class="widgets-area primary-sidebar service-sidebar ol-sm-12 col-md-4">
                                    <div class="widget_text widget">
                                        <h4 class="widget-title">Scarica PDF</h4>
                                        <div class="textwidget">
                                            <div class="download">
                                                <div class="item-download">
                                                    <a href="http://www.pacorig.com/storage/{{ json_decode($modulo->pdf)[0]->download_link }}"
                                                       target="_blank">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>Download
                                                        PDF
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        @endforeach

                </div>
                <hr>
            </div>

            <!-- sidebar -->

        </div>
    </div>
    </div>
    <!-- our services end -->

@endsection