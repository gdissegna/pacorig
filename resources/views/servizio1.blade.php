@extends('master')

@section('content')

 <!--page-header-->
    <div class="page-header has-image">
        <div class="page-header-content">
            <div class="featured-image-stoccaggio"></div>
            <div class="container">
                <h1>Stoccaggio rottami ferrosi e metalli</h1>
                <nav class="breadcrumbs">
                    <a class="home" href="#"><span>Home</span></a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <span>Servizi</span>
                </nav>
            </div>
        </div>
    </div>
    <!--page-header  end-->

<!-- projects -->
    <div class="single-project pagepadd">
        <div class="container">
            <div class="single-project-wrapper single-portfolio">
                <div class="entry-thumbnail  single-slide">
                    <div class="item">
                        <a href="#" class="photoswipe"><img src="images/services/pacorig-stoccaggio-rifiuti-slide-1.jpg" alt="" /></a>
                    </div>
                    <div class="item">
                            <a href="#" class="photoswipe"><img src="images/services/pacorig-intermediazione-slide-3.jpg" alt="" /></a>
                        </div>
                    <div class="item">
                            <a href="#" class="photoswipe"><img src="images/services/pacorig-stoccaggio-rifiuti-slide-3.jpg" alt="" /></a>
                        </div>
                </div>

                <h2 class="single-project-title">Stoccaggio rottami ferrosi, metalli e rifiuti non pericolosi</h2>
                <div class="row">
                    <div class="entry-content col-md-9 col-sm-12 col-xs-12">
                        <p>A seguito della sempre maggiore sensibilità rivolta alle problematiche ambientali ed intuendo la necessità di fornire risposte efficaci in tal senso, nel 1992 la nostra azienda avvia le pratiche per l’ottenimento dell’autorizzazione allo stoccaggio di rifiuti non pericolosi che viene conseguita nel 1994.</p>
                        <p>A partire da tale data all’attività storica di recupero di rottami metallici viene affiancata quella di raccolta e stoccaggio di rifiuti non pericolosi.</p>
                        <p>Operando principalmente per conto delle aziende presenti all’interno del cosiddetto “Distretto della sedia”, le tipologie di rifiuti trattate all’interno dell’impianto di stoccaggio sono prevalentemente composte dagli scarti misti di magazzino quali nylon estensibile LDPE, plastica, legno, carta e cartone, ferro ecc. e i residui provenienti dalle attività di verniciatura quali le morchie e i filtri delle cabine di verniciatura e gli imballaggi vuoti plastici e/o metallici.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- projects end -->

@endsection