<html>
    <head>
        @include('_partials.header')
    </head>
    <body class="home header-sticky header-v5 hide-topbar-mobile" >

    <div id="page" class="hfeed site">

        <!-- Preloader
        <div class="preloader"></div>
        -->
        <!-- topbar -->
        <div id="topbar" class="topbar hidden-md hidden-sm hidden-xs">
            <div class="container">

                <div class="topbar-left topbar-widgets text-left clearfix">
                    <div id="custom_html-9" class="widget_text widget widget_custom_html">
                        <div class="textwidget custom-html-widget">
                            <div><i class="flaticon-sign main-color"></i>Raccolta e trasporto rifiuti industriali / Commercio rottami ferrosi.</div>
                        </div>
                    </div>
                </div>

                <div class="topbar-right topbar-widgets text-right clearfix">
                    <div class="widget induscity-office-location-widget">
                        <div class="office-location clearfix">
                            
                            <ul class="office-1 topbar-office active" id="cargohub-office-1">
                                <li class="phone"><i class="flaticon-tool"></i>+(39) 0432.750721</li>
                                <li class="time"><i class="flaticon-time"></i>Lun - Ven: 08:00 - 12:30 / 13:30 - 17:30</li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- topbar end -->

        <!-- masthead -->
        <div id="mf-header-minimized" class="mf-header-minimized mf-header-v5"></div>
        <header id="masthead" class="site-header clearfix">
            <div class="header-main clearfix">
                <div class="site-contact">
                    <div class="container">
                        <div class="row menu-row">
                            <div class="site-logo col-md-9 col-sm-9 col-xs-9">
                                <a href="{{ route('home') }}" class="logo">
                                    <img src="images/logo.png" alt="Pacorig F.lli Logo" class="logo">
                                </a>
                                <h1 class="site-title"><a href="{{ route('home') }}" rel="home">Pacorig F.lli</a></h1>
                                <h2 class="site-description">Raccolta e trasporto rifiuti industriali - Commercio rottami ferrosi</h2>
                            </div>
                            <div class="site-extra-text hidden-md hidden-sm hidden-xs">

                                <div class="widget_text widget widget_custom_html">
                                    <div class="textwidget custom-html-widget">
                                        <div class="header-contact mail">
                                            <div>
                                                <h5>SCRIVETECI,</h5> <a href="mailto:info@pacorig.com">info@pacorig.com</a>
                                            </div>
                                            <i class="flaticon-note"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget_text widget widget_custom_html">
                                    <div class="textwidget custom-html-widget">
                                        <div class="header-contact mail">
                                            <div>
                                                <h5>Via Trieste n. 4,</h5> 33044 Manzano (UD)
                                            </div>
                                            <i class="flaticon-arrow"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="navbar-toggle col-md-3 col-sm-3 col-xs-3"><span id="mf-navbar-toggle" class="navbar-icon">
							<span class="navbars-line"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="site-menu container">
                    <nav id="site-navigation" class="main-nav primary-nav nav">
                        <ul class="menu">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('about') }}">Chi siamo</a></li>
                            <li class="has-children"><a href="" class="dropdown-toggle">Servizi</a>
                                <ul class="sub-menu">
                                    <li><a href="{{ route('servizio') }}">Raccolta e trasporto</a></li>
                                    <li><a href="{{ route('servizio1') }}">Stoccaggio</a></li>
                                    <li><a href="{{ route('servizio2') }}">Intermediazione</a></li>
                                    <li><a href="{{ route('servizio3') }}">Consulenza ambientale</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('autorizzazioni') }}">Autorizzazioni / Modulistica</a></li>
                            <li><a href="{{ route('contact') }}">Contatti</a></li>
                            
                        </ul>
                    </nav>

                </div>
            </div>
        </header>
        <!-- masthead end -->


    @yield('content')

    @include('_partials.footer')
    </body>
</html>