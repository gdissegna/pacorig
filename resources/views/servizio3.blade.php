@extends('master')

@section('content')

 <!--page-header-->
    <div class="page-header has-image">
        <div class="page-header-content">
            <div class="featured-image-consulenza-ambientale"></div>
            <div class="container">
                <h1>Consulenza</h1>
                <nav class="breadcrumbs">
                    <a class="home" href="#"><span>Home</span></a>
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    <span>Servizi</span>
                </nav>
            </div>
        </div>
    </div>
    <!--page-header  end-->

<!-- projects -->
    <div class="single-project pagepadd">
        <div class="container">
            <div class="single-project-wrapper single-portfolio">
                <div class="entry-thumbnail">
                    <div class="item">
                        <img src="images/services/pacorig-consulenza-ambientale-slide-1.jpg" alt="" />
                    </div>
                </div>

                <h2 class="single-project-title">Consulenza ambientale</h2>
                <div class="row">
                    <div class="entry-content col-md-9 col-sm-12 col-xs-12">
                        <p>La nascita nel 1999 della GEA di Pacorig Manuela amplia la gamma dei nostri servizi.</p>
                        <p>I servizi svolti dalla GEA sono finalizzati alla risoluzione di tutte le problematiche di carattere amministrativo inerenti il settore ambientale quali ad esempio la tenuta dei registri di carico e scarico rifiuti, la redazione del MUD, la redazione di pratiche autorizzative (emissioni in atmosfera, inscrizione CONAI, iscrizione all’Albo Gestori Ambientali, ecc.) e relative a sistemi di gestione, audit, conformità normativa ecc.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- projects end -->

@endsection