@extends('master')

@section('title')

@section('content')

    <!--Main Slider-->
    <section class="rev_slider_wrapper">
        <div id="slider1" class="rev_slider" data-version="5.0">

            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="images/slide1-h1-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="/images/main-slider-1.jpg"  alt="" title="slide1-h1.jpg"   data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme rev-midtext"
                         id="slide-1-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['15','295','15','15']" 
							 data-y="['top','top','top','top']" data-voffset="['195','195','295','95']"
                         data-fontsize="['30','30','30','22']"
                         data-lineheight="['30','30','30','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":300,"speed":600,"text_c":"transparent",
							"bg_c":"transparent","use_text_c":false,"use_bg_c":false,
							"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},
							{"delay":"wait","speed":600,"use_text_c":false,"use_bg_c":false,
							"text_c":"transparent","bg_c":"transparent","frame":"999",
							"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                    >BENVENUTI ALLA PACORIG F.LLI </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption   tp-resizeme rev-bigtext"
                         id="slide-1-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['15','295','15','15']" 
							 data-y="['top','top','top','top']" data-voffset="['245','245','345','160']"
                         data-fontsize="['70','70','70','30']"
                         data-lineheight="['30','30','30','26']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":500,"speed":600,"text_c":"transparent",
							"bg_c":"transparent","use_text_c":false,"use_bg_c":false,
							"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},
							{"delay":"wait","speed":600,"use_text_c":false,"use_bg_c":false,
							"text_c":"transparent","bg_c":"transparent","frame":"999",
							"to":"x:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                    >60 ANNI DI ESPERIENZA</div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption   tp-resizeme rev-bigtext"
                         id="slide-1-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['15','295','15','15']" 
							 data-y="['top','top','top','top']" data-voffset="['325','325','425','220']"
                         data-fontsize="['70','70','70','30']"
                         data-lineheight="['30','30','30','26']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":700,"speed":600,"text_c":"transparent",
							"bg_c":"transparent","use_text_c":false,"use_bg_c":false,
							"frame":"0","from":"x:50px;opacity:0;","to":"o:1;",
							"ease":"Power3.easeInOut"},{"delay":"wait","speed":600,
							"use_text_c":false,"use_bg_c":false,"text_c":"transparent",
							"bg_c":"transparent","frame":"999","to":"x:50px;opacity:0;",
							"ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                    >AL VOSTRO SERVIZIO</div>

                
                <!-- SLIDE  -->
                <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="images/slide2-h1-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="images/main-slider-2.jpg"  alt="" title="slide2-h1.jpg"   data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme rev-midtext"
                         id="slide-2-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['15','295','15','15']"
                         data-y="['top','top','top','top']" data-voffset="['195','195','295','95']"
                         data-fontsize="['30','30','30','22']"
                         data-lineheight="['30','30','30','22']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":300,"speed":600,"text_c":"transparent",
							"bg_c":"transparent","use_text_c":false,"use_bg_c":false,
							"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},
							{"delay":"wait","speed":600,"use_text_c":false,"use_bg_c":false,
							"text_c":"transparent","bg_c":"transparent","frame":"999",
							"to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                    >BENVENUTI ALLA PACORIG F.LLI </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption   tp-resizeme rev-bigtext"
                         id="slide-2-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['20','300','20','20']"
                         data-y="['top','top','top','top']" data-voffset="['245','245','345','160']"
                         data-fontsize="['70','70','70','30]"
                         data-lineheight="['30','30','30','26']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":900,"speed":600,"text_c":"transparent",
							"bg_c":"transparent","use_text_c":false,"use_bg_c":false,
							"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"}
							,{"delay":"wait","speed":600,"use_text_c":false,
							"use_bg_c":false,"text_c":"transparent","bg_c":"transparent",
							"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                    >PER LA TUTELA </div>



                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme rev-bigtext"
                         id="slide-2-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['20','300','20','20']"
                         data-y="['top','top','top','top']" data-voffset="['325','325','425','220']"
                         data-fontsize="['70','70','70','30']"
                         data-lineheight="['30','30','30','26']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":900,"speed":600,"text_c":"transparent",
							"bg_c":"transparent","use_text_c":false,"use_bg_c":false,
							"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"}
							,{"delay":"wait","speed":600,"use_text_c":false,
							"use_bg_c":false,"text_c":"transparent","bg_c":"transparent",
							"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                    >DELL'AMBIENTE </div>


                </li>
                </li>
                <!-- SLIDE  -->
            </ul>
        </div>
    </section>
    <!--Main Slider  end-->

<!-- About us-->
        <div class="about-company">
            <div class="container">
                <div class="mf-section-title text-left dark large-size margbtm40 ">
                    <h2>A proposito di noi...</h2></div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="aboutcoimg">
                            <img src="images/pacorig-manzano-rottami-ferrosi-about.jpg" alt="F.lli Pacorig - visuale dello stabilimento" title="F.lli Pacorig - visuale dello stabilimento">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-8">
                        <div class="aboutcoinfo">
                            <p>La storia della nostra azienda ha inizio nel lontano 1957 quando la famiglia Pacorig dà il via all’attività di commercio di rottami ferrosi e metalli.</p>
                            <p>A seguito della sempre maggiore sensibilità rivolta alle problematiche ambientali ed intuendo la necessità di fornire risposte efficaci in tal senso, nell’anno 1994 abbiamo affiancato all’attività originaria quella di stoccaggio di rifiuti non pericolosi.</p>
                            <p>Operando principalmente per conto delle aziende presenti all’interno del cosiddetto “Distretto della sedia”, le tipologie di rifiuti trattate all’interno dell’impianto di stoccaggio sono prevalentemente composte dagli scarti misti di magazzino e dai rifiuti provenienti dalle attività di verniciatura.</p>
                            <p>I servizi di raccolta e trasporto vengo da sempre effettuati con automezzi di proprietà debitamente autorizzati alle categorie 4 (rifiuti non pericolosi) e 5 (rifiuti pericolosi). Tutti i mezzi sono dotati di impianto scarrabile per la movimentazione di cassoni e di gru idraulica per il carico dei rifiuti sfusi. La nostra azienda è dotata inoltre di varie attrezzature per il contenimento di rifiuti che comprendono tra gli altri circa 100 cassoni la maggior parte dei quali a noleggio presso i nostri clienti.</p>
                            <p>Dal 2012, a completamento dei nostri servizi, abbiamo iniziato l’attività di intermediazione di rifiuti pericolosi e non pericolosi.</p>
                            <p>Ci occupiamo inoltre della fornitura di imballaggi per rifiuti sfusi quali fusti metallici e sacchi big-bags di vario genere per rifiuti non pericolosi e pericolosi.</p>
                            <p>Nel 1999, anno di nascita della GEA di Pacorig Manuela, l'attività si espande con la fornitura di servizi di assistenza e consulenza nel settore ambientale.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About us end-->


<!-- our services -->
        <div class="service-2">
            <div class="container">
                <div class="mf-section-title text-center dark large-size margbtm40">
                    <h2>I nostri servizi</h2></div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="mf-services-2 icon_type-flaticon style-1 ">
                            <div class="service-thumbnail">
                                <a href="{{ route('servizio') }}">
                                    <span class="service-icon"><i class="fa fa-link"></i></span>
                                    <img class="" src="images/services/pacorig-mezzi-raccolta-trasporto-rifiuti.jpg" width="370" height="200" alt="F.lli Pacorig - mezzi per la raccolta e il trasporto dei rifiuti" title="F.lli Pacorig - mezzi per la raccolta e il trasporto dei rifiuti">
								</a>
                            </div>
                            <div class="service-summary">
                                <h4><a href="{{ route('servizio') }}" title="Raccolta e trasporto">Raccolta e trasporto</a></h4>
                                
                                <a href="{{ route('servizio') }}" title="Leggi" class="btn-service-2">Leggi<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="mf-services-2 icon_type-flaticon style-1 ">
                            <div class="service-thumbnail">
                                <a href="{{ route('servizio1') }}">
                                    <span class="service-icon"><i class="fa fa-link"></i></span>
                                    <img class="" src="images/services/pacorig-stoccaggio-rifiuti.jpg" width="370" height="200" alt="F.lli Pacorig - stoccaggio dei rifiuti" title="F.lli Pacorig - stoccaggio dei rifiuti">
								</a>
                            </div>
                            <div class="service-summary">
                                <h4><a href="{{ route('servizio1') }}" title="Stoccaggio">Stoccaggio rifiuti</a></h4>
                                
                                <a href="{{ route('servizio1') }}" title="Leggi" class="btn-service-2">Leggi<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="mf-services-2 icon_type-flaticon style-1 ">
                            <div class="service-thumbnail">
                                <a href="{{ route('servizio2') }}">
                                    <span class="service-icon"><i class="fa fa-link"></i></span>
                                    <img class="" src="images/services/pacorig-intermediazione.jpg" width="370" height="200" alt="F.lli Pacorig - intermediazione" title="F.lli Pacorig - intermediazione">
								</a>
                            </div>
                            <div class="service-summary">
                                <h4><a href="{{ route('servizio2') }}" title="Intermediazione">Intermediazione</a></h4>
                                
                                <a href="{{ route('servizio2') }}" title="Leggi" class="btn-service-2">Leggi<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <div class="mf-services-2 icon_type-flaticon style-1 ">
                            <div class="service-thumbnail">
                                <a href="{{ route('servizio3') }}">
                                    <span class="service-icon"><i class="fa fa-link"></i></span>
                                    <img class="" src="images/services/pacorig-consulenza-ambientale.jpg" width="370" height="200" alt="F.lli Pacorig - consulenza ambientale" title="F.lli Pacorig - consulenza ambientale">
								</a>
                            </div>
                            <div class="service-summary">
                                <h4><a href="{{ route('servizio3') }}" title="Consulenza ambientale">Consulenza ambientale</a></h4>
                                
                                <a href="{{ route('servizio3') }}" title="Leggi" class="btn-service-2">Leggi<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- our services end -->


@endsection