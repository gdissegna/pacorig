<?php

namespace App;

use App\BaseModel;


class Authorization extends BaseModel
{

    public function AuthorizationCategory() {
        return $this->belongsTo('App/AuthorizationCategory', 'category_id');
    }
    
}
