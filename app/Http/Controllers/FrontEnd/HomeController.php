<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servizio;


class HomeController extends Controller
{
    public $services;
	/**
	 * HomeController constructor.
	 */
	public function __construct(Servizio $service) {
		$this->services = $service;
	}

	public function index() {

	   $servizi =  $this->services->all();

		return view('index',["servizi" => $servizi]);
	}



}
