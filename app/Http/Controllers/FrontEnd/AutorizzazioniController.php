<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Authorization;
use App\AuthorizationsCategory;
use App\Module;

class AutorizzazioniController extends Controller
{
    protected $autorizazzioni;
    protected $authoriationscategory;
    protected $moduli;

    public function __construct(Authorization $authorization, AuthorizationsCategory $autorizationscategory, Module $moduli) {

        $this->autorizazzioni = $authorization;
        $this->autorizzazionicategoria = $autorizationscategory;
        $this->moduli = $moduli;
    }

    public function index() {

        $autolist = $this->autorizzazionicategoria->all();

        $modulilist = $this->moduli->all();



        return view('autorizzazioni' , ['autolist' => $autolist , 'modulilist' => $modulilist]);
    }

    public function download($id) {

        $autodownload = $this->autorizazzioni->find($id);

        return response()->download($autodownload->pdf["download_link"], $autodownload->pdf['filename']);
    }

    public function downloadmodulo($id) {

        $modulodownload = $this->moduli->find($id);

        return response()->download($modulodownload->pdf["download_link"], $modulodownload->pdf['filename']);
    }

}
