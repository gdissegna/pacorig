<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Servizio;

class ServiziController extends Controller
{
    protected $servizio;

    public function __construct(Servizio $servizio)
    {
        $this->servizio = $servizio;
    }

    public function ChiSiamo(){

        return view('chisiamo');
    }

    public function Servizio() {

        return view('servizio');
    }



}
