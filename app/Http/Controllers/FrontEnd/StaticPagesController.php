<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPagesController extends Controller
{
   public function ChiSiamo() {

       return view('chisiamo');
   }

   public function Servizio() {

       return view('servizio');
   }

    public static function Servizio_1() {

        return view('servizio1');
    }

    public static function Servizio_2() {

        return view('servizio2');
    }

    public static function Servizio_3() {

        return view('servizio3');
    }

    public static function privacy() {

       return view('privacy');
    }

}
