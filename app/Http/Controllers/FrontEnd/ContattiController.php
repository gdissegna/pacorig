<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactFormRequest;
use App\Http\Controllers\Controller;
use App\Mail\contact;
use App\Mail\contactinternal;



class ContattiController extends Controller
{
	protected $ContactFormRequest;
	/**
	 * ContattiController constructor.
	 */
	public function __construct() {

	}

	public function Contact() {
        return view('contact');
	}

    /**
     * @param ContactFormRequest $contactFormRequest
     */
    public function ContactPost(ContactFormRequest $contactFormRequest) {



        mail::to($contactFormRequest['email'])
            ->send(new contact($contactFormRequest));


        mail::to('info@pacorig.com')
            ->send(new contactinternal($contactFormRequest));

       /*
        Mail::send('Mail.Contact', ['request' => $this->ContactFormRequest], function ($message) {
            $message->from('info@triangolo.it', 'Pacorig');
            $message->to($this->ContactFormRequest['email'], $this->ContactFormRequest['name']);
            $message->subject('pacorig, ' . 'Grazie per averci contattati');
        });

        Mail::send('Mail.ContactInternal', ['request' => $this->ContactFormRequest], function ($message) {
            $message->from('info@Otriangolo.it', 'Pacorig');
            $message->to('info@triangolo.it');
            $message->subject('Pacorig nuovo contatto');
        });
        */

       $contactFormRequest->session()->flash('success','La email è stata inviata');

        redirect()->route('contact');

	}
}
