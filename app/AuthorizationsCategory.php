<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class AuthorizationsCategory extends Model
{

    public function parents(){
       return $this->where('parent_id', null)->all();
    }

    public function subcategory() {
        return $this->hasMany('App/AuthorizationsCategory');
    }

    public function parent(){
        return $this->belongsTo('App/AuthorizationsCategory', 'parent_id');
    }

    public function Authorization() {
        return $this->hasMany(\App\Authorization::class,'category_id', 'id');
    }
}
