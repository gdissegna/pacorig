<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Requests\ContactFormRequest;

class contactinternal extends Mailable
{
    use Queueable, SerializesModels;

    public $testomail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactFormRequest $contactFormRequest)
    {
        $this->testomail = $contactFormRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this
            ->subject($this->testomail['subject'])
            ->from('info@pacorig.com')
            ->view('Mail.ContactInternal');
    }
}
